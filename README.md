# PrimerServidorHTTP

Primer servidor utilizando HTTP.

## Iniciando

Sigue las siguientes instrucciones para tener una copia del proyecto en tu maquina local, poder correrlo, probarlo y modificarlo.

### Prerrequisitos

- [ ] Git
- [ ] Node.js

### Instalando

Para clonar el repositorio utiliza los siguientes comandos:

```
cd carpeta
git init
git remote add origin https://gitlab.com/emi154loya/primerServidorHTTP.git
git pull origin main
```

### Uso

Para correr el programa sigue los siguientes comandos:

```
cd carpeta_repositorio
node app.js
```

## Hecho con

- [ ] HTML
- [ ] Node.js
- [ ] JavaScript

## Version

```
0.0.1
```

## Authors

- [ ] Emiliano Loya Flores 357611
