const http = require("http");
const fs = require("fs");

// HTTP:
// En el request hay 2 elementos:
//      Header:
//          - Hacia donde va la petición (URL)
//          - Metodo del metodo HTTP al que va la petición (GET, POST, PATCH, PUT, DELETE)
//      Body:
//          - Viene información adicional
http
  .createServer((req, res) => {
    const file = req.url == "/" ? "./WWW/index.html" : `./WWW/${req.url}`;
    fs.readFile(file, (err, data) => {
      if (err) {
        // Mandar 404
        res.writeHead(400, { "Content-Type": "text/html" });
        res.write("Not found");
        res.end();
      } else {
        const extension = req.url.split(".").pop();
        switch (extension) {
          case "txt":
            res.writeHead(200, { "Content-Type": "text/plain" });
            break;
          case "html":
            res.writeHead(200, { "Content-Type": "text/html" });
            break;
          case "jpg":
            res.writeHead(200, { "Content-Type": "image/jpg" });
            break;
        }
        res.write(data);
        res.end();
        // res.writeHead(200, { "Content-Type": "text/html" });
        // res.write(data);
        // res.end();
      }
    });
  })
  .listen(4444);
